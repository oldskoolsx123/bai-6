package com.example.bai6.Model.Entity;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data

public class Student extends Person {
    private String lop;
    private int diem;

    public String getLop() {
        return lop;
    }

    public void setLop(String lop) {
        this.lop = lop;
    }

    public int getDiem() {
        return diem;
    }

    public void setDiem(int diem) {
        this.diem = diem;
    }

    public Student(int tuoi, String ten, String gioiTinh) {
        super(tuoi, ten, gioiTinh);
        //cach su dung super: super(thuoc tinh class cha) de truyen vao cac thuoc tinh can thiet de chay constructor cua Person
    }

    public Student(int tuoi, String ten, String gioiTinh, String lop, int diem) {
        super(tuoi, ten, gioiTinh);
        this.lop = lop;
        this.diem = diem;
    }

    public Student(int tuoi, String ten, String lop, int diem) {
        super(tuoi, ten);
        this.lop = lop;
        this.diem = diem;
    }

    public Student(String lop, int diem) {
        this.lop = lop;
        this.diem = diem;
    }

    public Student() {
    }

    public Student(String lop, String ten) {
        this.lop = lop;
    }

    public Student(String lop, String ten, int diem) {
        this.lop = lop;
        this.diem = diem;
    }

    public Student createStudentNoParams() {
        Student student = new Student();
        System.out.println(student);
        return student;
    }

    public Map<String, String> createStudentWithLopAndTen() {
        Student student = new Student("11C","Minh");
        System.out.println(student.lop);
        Map<String, String> studentMap = new HashMap<>();
        studentMap.put("lop", student.lop);
        studentMap.put("ten", student.getTen());
        return studentMap;
    }

    public Map<String, Object> createStudentWithAgeNameAndGender() {
        Student student = new Student(16,"Minh","Nam");
        Map<String, Object> studentMap = new HashMap<>();
        studentMap.put("tuoi", student.getTuoi());
        studentMap.put("ten", student.getTen());
        studentMap.put("diem", student.diem);
        return studentMap;
    }

    public Map<String, Object> createStudentWithAllAttributes(){
        Student student = new Student("11C","Minh", 7);
        Map<String, Object> studentMap = new HashMap<>();
        studentMap.put("lop", student.lop);
        studentMap.put("ten", student.getTen());
        studentMap.put("diem", student.diem);
        return studentMap;
    }

    public String sayHello() {
        System.out.println("My info is secret");
        return "My info is secret";
    }
    public String sayHello(int tuoi, String ten) {
        System.out.println("Hello, I am a student, my name "+ ten +", i am " + tuoi +" years old");
        return "Hello, I am a student, my name is "+ ten +", i am " + tuoi +" years old";
    }
    public String sayHello(int tuoi) {
        System.out.println("Hello, I am a student,  i am "+ tuoi +" years old");
        return "Hello, I am a student,  i am "+ tuoi +" years old";
    }

//    @Override
//    public void hello() {
//        // Implement the behavior for the hello() method in the Student class
//        System.out.println("Hello, I am a student.");
//    }
//    Khong the override duoc method Hello vi no co access private
}
