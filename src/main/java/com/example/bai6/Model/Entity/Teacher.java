package com.example.bai6.Model.Entity;

import lombok.Data;

@Data
public class Teacher extends Person {
    private int namKinhNghiem;
    private boolean honNhan;

    public Teacher() {
    }

    public Teacher(int tuoi, String ten, String gioiTinh, int namKinhNghiem, boolean honNhan) {
        super(tuoi, ten, gioiTinh);
        this.namKinhNghiem = namKinhNghiem;
        this.honNhan = honNhan;
    }

    public Teacher(int tuoi, String ten, String gioiTinh, boolean honNhan) {
        super(tuoi, ten, gioiTinh);
        this.honNhan = honNhan;
    }

    public int getNamKinhNghiem() {
        return namKinhNghiem;
    }

    public boolean isHonNhan() {
        return honNhan;
    }
}
