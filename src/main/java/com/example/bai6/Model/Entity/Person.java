package com.example.bai6.Model.Entity;

import lombok.Data;

@Data
public class Person {
    private int tuoi;
    private String ten;
    private String gioiTinh;

    public int getTuoi() {
        return tuoi;
    }

    public String getTen() {
        return ten;
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public Person(int tuoi, String ten, String gioiTinh) {
        this.tuoi = tuoi;
        this.ten = ten;
        this.gioiTinh = gioiTinh;
    }

    public Person(int tuoi, String ten) {
        this.tuoi = tuoi;
        this.ten = ten;
        this.gioiTinh = "woke";
    }

    public Person() {
    }

    public String sayHello() {
        Person person = new Person();
        System.out.println("Hello I'm a person");
        return null;
    }

    private void hello() {
        System.out.println("Person here");
    }
}
