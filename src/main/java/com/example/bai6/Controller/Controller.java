package com.example.bai6.Controller;

import com.example.bai6.Service.StudentInter;
import com.example.bai6.Service.TeacherInter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
@CrossOrigin
@RequestMapping("/calling")
public class Controller {

//    Url:
//    http://localhost:8080/calling/sayHelloStudent
//    http://localhost:8080/calling/introduceStudent?age=16&name=Minh

    @Autowired
    private StudentInter studentInter;

    @Autowired
    private TeacherInter teacherInter;

    @GetMapping("/getAllStudent")
    public ResponseEntity<?> getAllStudent() {
        return new ResponseEntity<>(studentInter.getAllStudent(), HttpStatus.OK);
    }

    @GetMapping("/countAllStudent")
    public ResponseEntity<?> countAllStudent() {
        return new ResponseEntity<>(studentInter.countAllStudent(), HttpStatus.OK);
    }

    @PostMapping("/createStudent")
    public ResponseEntity<?> createStudent(@RequestParam int tuoi,
                                           @RequestParam String ten,
                                           @RequestParam String gioiTinh,
                                           @RequestParam String lop,
                                           @RequestParam int diem) {
        return new ResponseEntity<>(studentInter.createStudent(tuoi, ten, gioiTinh, lop, diem), HttpStatus.OK);
    }

    @PutMapping("/updateStudent")
    public ResponseEntity<?> updateStudent(@RequestParam int tuoi,
                                           @RequestParam String ten,
                                           @RequestParam String gioiTinh,
                                           @RequestParam String lop,
                                           @RequestParam int diem,
                                           @RequestParam int id) {
        return new ResponseEntity<>(studentInter.updateStudent(id, tuoi, ten, gioiTinh, lop, diem), HttpStatus.OK);
    }

    @GetMapping("/getAllTeacher")
    public ResponseEntity<?> getAllTeacher() {
        return new ResponseEntity<>(teacherInter.getAllTeacher(), HttpStatus.OK);
    }

    @GetMapping("/countAllTeacher")
    public ResponseEntity<?> countAllTeacher() {
        return new ResponseEntity<>(teacherInter.countAllTeacher(), HttpStatus.OK);
    }

    @PostMapping("/createTeacher")
    public ResponseEntity<?> createTeacher(@RequestParam int tuoi,
                                           @RequestParam String ten,
                                           @RequestParam String gioiTinh,
                                           @RequestParam int namKinhNghiem,
                                           @RequestParam boolean honNhan) {
        return new ResponseEntity<>(teacherInter.createTeacher(tuoi, ten, gioiTinh, namKinhNghiem, honNhan), HttpStatus.OK);
    }

    @PutMapping("/updateTeacher")
    public ResponseEntity<?> updateTeacher(@RequestParam int tuoi,
                                           @RequestParam String ten,
                                           @RequestParam String gioiTinh,
                                           @RequestParam int id,
                                           @RequestParam int namKinhNghiem,
                                           @RequestParam boolean honNhan) {
        return new ResponseEntity<>(teacherInter.updateTeacher(id, tuoi, ten, gioiTinh, namKinhNghiem, honNhan), HttpStatus.OK);
    }
}
