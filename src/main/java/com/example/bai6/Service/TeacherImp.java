package com.example.bai6.Service;

import com.example.bai6.Model.Entity.Student;
import com.example.bai6.Model.Entity.Teacher;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class TeacherImp implements TeacherInter {

    List<Teacher> teacherList = new ArrayList<>();

    @Override
    public List<Teacher> getAllTeacher() {
        return teacherList;
    }

    @Override
    public int countAllTeacher() {
        int teacherCount = teacherList.size();
        return teacherCount;
    }

    public String createTeacher(int tuoi, String ten, String gioiTinh, int namKinhNghiem, boolean honNhan) {
        teacherList.add(new Teacher(tuoi, ten, gioiTinh, namKinhNghiem, honNhan));
        return "Thêm thành công";
    }

    @Override
    public String updateTeacher(int id, int tuoi, String ten, String gioiTinh, int namKinhNghiem, boolean honNhan) {
        try {
            Optional<Teacher> teacher = Optional.empty();
            if (id >= 0 && id < teacherList.size()) {
                teacher = Optional.of(teacherList.get(id));
            }
            if(teacher.isPresent()){
                Teacher existingTeacher = teacher.get();
                existingTeacher.setTen(ten);
                existingTeacher.setTuoi(tuoi);
                existingTeacher.setGioiTinh(gioiTinh);
                existingTeacher.setHonNhan(honNhan);
                existingTeacher.setNamKinhNghiem(namKinhNghiem);
                return "Sửa thành công";
            }
            return "Failed";
        }catch (Exception e){
            return e.toString();
        }
    }
}