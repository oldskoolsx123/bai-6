package com.example.bai6.Service;

import com.example.bai6.Model.Entity.Teacher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface TeacherInter {

    public List<Teacher> getAllTeacher();

    public String updateTeacher(int id, int tuoi, String ten, String gioiTinh, int namKinhNghiem, boolean honNhan);

    public String createTeacher(int tuoi, String ten, String gioiTinh, int namKinhNghiem, boolean honNhan);

    public int countAllTeacher();
}
