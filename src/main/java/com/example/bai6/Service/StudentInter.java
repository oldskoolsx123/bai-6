package com.example.bai6.Service;

import com.example.bai6.Model.Entity.Student;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface StudentInter {
    public List<Student> getAllStudent();

    public int countAllStudent();

    public String createStudent(int tuoi, String ten, String gioiTinh, String lop, int diem);

    public String updateStudent(int id, int tuoi, String ten, String gioiTinh, String lop, int diem);
}
