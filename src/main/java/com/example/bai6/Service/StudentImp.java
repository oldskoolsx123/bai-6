package com.example.bai6.Service;

import com.example.bai6.Model.Entity.Student;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class StudentImp implements StudentInter {
    List<Student> studentList = new ArrayList<>();

    @Override
    public List<Student> getAllStudent() {
        return studentList;
    }

    @Override
    public int countAllStudent() {
        int studentCount = studentList.size();
        return studentCount;
    }

    public String createStudent(int tuoi, String ten, String gioiTinh, String lop, int diem) {
        studentList.add(new Student(tuoi, ten, gioiTinh, lop, diem));
        return "Thêm thành công";
    }

    @Override
    public String updateStudent(int id, int tuoi, String ten, String gioiTinh, String lop, int diem) {
        try {
            Optional<Student> student = Optional.empty();
            if (id >= 0 && id < studentList.size()) {
                student = Optional.of(studentList.get(id));
            }
            if(student.isPresent()){
                Student existingStudent = student.get();
                existingStudent.setTen(ten);
                existingStudent.setTuoi(tuoi);
                existingStudent.setGioiTinh(gioiTinh);
                existingStudent.setLop(lop);
                existingStudent.setDiem(diem);
                return "Sửa thành công";
            }
            return "Failed";
        }catch (Exception e){
            return e.toString();
        }
    }
}
